#!/bin/bash

#####################################################################################################
## Этот скрипт предназначен для очистки Исторической БД во время выполнения плановых работ.
## Запускается без параметров и выполняется самостоятельно.
## Все логи выполнения выводятся на монитор во время выполнения.
##
## Разработан Капущак М.М.
## Начало разработки 26.06.2023
#####################################################################################################


####### -------------------------------------------- VARS, FUNCTIONS & OTHER -------------------------------------------- #######
####### ----------- VARS ----------- #######

SCRIPT=$0
CUR_USER=whoami
DB2INST1="db2inst1"

## Таблицы для очистки
REPSTAT=REPORTER_STATUS
REPJOUR=REPORTER_JOURNAL
REPDET=REPORTER_DETAILS
REPAOGID=REP_AUDIT_OWNERGID
REPAOUID=REP_AUDIT_OWNERUID
REPACORR=REP_AUDIT_CORR    #*
REPASEV=REP_AUDIT_SEVERITY    #*
REPAACK=REP_AUDIT_ACK

CUR_MONTH=$(date '+%m')    # текущий месяц
CUR_YEAR=$(date '+%Y')    # текущий год
let "CUR_MONTH -= 1"    # текущий месяц - 1
MONTH_NUM=$((12-CUR_MONTH))    # номер месяца для отсчёта

S_TIME=15    # время отдыха между проверками на "db2 list utilities show detail" & "TABLESPACE ... REDUCE MAX"

# имена временных файлов для записи списка таблиц на удаление
FILE_REPSTAT=${REPSTAT}"_PART_"${CUR_YEAR}.temp_hdbc    # temp_hdbc - означает temporary_history-database-clean
FILE_REPJOUR=${REPJOUR}"_PART_"${CUR_YEAR}.temp_hdbc
FILE_REPDET=${REPDET}"_PART_"${CUR_YEAR}.temp_hdbc
FILE_REPAOGID=${REPAOGID}"_PART_"${CUR_YEAR}.temp_hdbc
FILE_REPAOUID=${REPAOUID}"_PART_"${CUR_YEAR}.temp_hdbc
FILE_REPACORR=${REPACORR}"_PART_"${CUR_YEAR}.temp_hdbc
FILE_REPASEV=${REPASEV}"_PART_"${CUR_YEAR}.temp_hdbc
FILE_REPAACK=${REPAACK}"_PART_"${CUR_YEAR}.temp_hdbc

####### ----------- FUNCTIONS ----------- #######

## Функция с входными параметрами для открепления партиций и удаления таблиц.
#+ первый параметр - имя файла со списком партиций
#+ второй параметр - имя таблицы
alter_table ()
{
#  for TABLE_PART in $(cat "$1");
  for TABLE_PART in $(head -n 1 "$1");    # в тестовом режиме сделал пока удаление 3х самых старых таблиц
  do
    echo "<<<---detach partition ${TABLE_PART}...";
    db2 "alter table "$2"  detach partition ${TABLE_PART} into table ${TABLE_PART} " && db2 " drop table ${TABLE_PART} " ;
    echo ">>>---get state"
      state=$(db2 list utilities show detail | grep -i State | cut -d "=" -f 2)
      echo $state " for debug"

      while [ $state != "00000" ]
      do
        echo "<<<---sleep ${S_TIME} - Ожидаем выполнения предыдущих операций alter table & drop table"
        sleep $S_TIME
        echo ">>>---get state (in While) for debug"
        state=$(db2 list utilities show detail | grep -i State | cut -d "=" -f 2)
        echo $state " (in While) for debug"
      done;
  done
}

## Функция с входными параметрами для проверки состояния выполнения
#+ запроса TABLESPACE ... REDUCE MAX.
#+ первый параметр - имя TABLESPACE
get_extent_movement_status()
{
db2 "select TBSP_NAME, NUM_EXTENTS_LEFT from table(SYSPROC.MON_GET_EXTENT_MOVEMENT_STATUS('', -1))" | grep ${1} | sed 's/  */ /g' | cut -d " " -f 2 > ${1}.temp_hdbc

echo ">>>---in ${1} NUM_EXTENTS_LEFT"
echo ">>>---${1} "
for NUM_EXTENTS_LEFT in $(cat ${1}.temp_hdbc);
do
  echo $NUM_EXTENTS_LEFT
  if (( $NUM_EXTENTS_LEFT != '-1' )); then
    echo "!!!---Не равняется (-1)= " $NUM_EXTENTS_LEFT
    echo "<<<---sleep ${S_TIME} - Ожидаем завершения TABLESPACE ${1} REDUCE MAX "
    sleep $S_TIME
    while (( $NUM_EXTENTS_LEFT != '-1' ))
    do
      echo ">>>---get NUM_EXTENTS_LEFT (in While)"
      db2 "select TBSP_NAME, NUM_EXTENTS_LEFT from table(SYSPROC.MON_GET_EXTENT_MOVEMENT_STATUS('', -1))" | grep ${1} | sed 's/  */ /g' | cut -d " " -f 2 > ${1}.temp_hdbc
      NUM_EXTENTS_LEFT=$(cat ${1}.temp_hdbc)
      echo ">>>---NUM_EXTENTS_LEFT=${NUM_EXTENTS_LEFT} (in While)"
      echo "<<<---sleep ${S_TIME} - Ожидаем завершения TABLESPACE ${1} REDUCE MAX (in While)"
      sleep $S_TIME
#      echo ">>>---get NUM_EXTENTS_LEFT (in While)"
#      db2 "select TBSP_NAME, NUM_EXTENTS_LEFT from table(SYSPROC.MON_GET_EXTENT_MOVEMENT_STATUS('', -1))" | grep ${1} | sed 's/  */ /g' | cut -d " " -f 2 > ${1}.temp_hdbc
#      NUM_EXTENTS_LEFT=$(cat ${1}.temp_hdbc)
#      echo ">>>---NUM_EXTENTS_LEFT=${NUM_EXTENTS_LEFT} (in While)"
    done;
  else
    echo ">>>---Равняется (-1)= " $NUM_EXTENTS_LEFT
  fi;
done
}


### ----------- DEBUG
#echo $(whoami)
#echo $SCRIPT

####### ----------- SWITCH user to db2inst1 ----------- #######

if [ $(whoami) != "$DB2INST1" ]
then
#  echo "<<<---SWITCH user to db2inst1. Please enter password..."
#  sudo su - db2inst1
  echo ">>>---Текущий пользователь не db2inst1. Запустите сценарий под оболочкой db2inst1."
  exit 1
else
  echo "<<<---Текущий пользователь db2inst1. Продолжаем выполнение..."
fi


####### -------------------------------------------- BEGIN CONNECT  -------------------------------------------- #######

echo "<<<------ Начало выполнения сценария автоматической очитски ИБД...  ------>>>"

####### ----------- CONNECT ----------- #######
echo "<<<---connect to reporter..."
db2 connect to REPORTER

####### ----------- UNCATALOG - CATATLOG ----------- #######
echo "<<<---change mode to (mnt)..."
## получение точки монтирования БД
LDD=$(db2 list db directory | grep REPORTER -A2 | grep "Local database directory" | cut -d "/" -f 2)
## меняем алиас БД
db2 uncatalog database REPORTER
db2 catalog database REPORTER as mnt on "/"$LDD
db2stop force
db2set DB2_PMODEL_SETTINGS=MAX_BACKGROUND_SYSAPPS:1
db2start
####### ----------- CONNECT ----------- #######
db2 connect to mnt

## Проверка на коннект к новому расположению БД
DB_ALIAS=$(db2 list db directory | grep MNT -A2 | grep "Database alias" | cut -d "=" -f 2)
echo $DB_ALIAS
if [ "$DB_ALIAS" != " MNT"  ]; then
  echo "!!!---алиас БД не изменился на MNT !!! Значит что-то пошло не так. Нужно перепроверить вручную."
  exit 1
else
  echo "<<<---OK. Алиас БД = MNT"
fi

####### -------------------------------------------- START clean -------------------------------------------- #######

### ----------- DEBUG
#echo "CUR_MONTH=" + $CUR_MONTH
#echo "CUR_YEAR=" + $CUR_YEAR

### ----------- LIST partitions

## получение списка партиций для удаления кроме последних 13 месяцев
#+ для всех таблиц

### ----------------------------- REPORTER_STATUS --------------------------------
echo "<<<---ОЧИСТКА REPORTER_STATUS..."
# Формирование списка таблиц старше 13-ти месяцев на удаление.
db2look -d mnt -e -t $REPSTAT | grep ${REPSTAT}"_PART_" | grep -v ${REPSTAT}"_PART_"${CUR_YEAR} | cut -d " " -f 3 | sed -e 's/^"//' -e 's/"$//' | head -n -$MONTH_NUM > ${FILE_REPSTAT}
# Вывод списка таблиц старше 13-ти месяцев на удаление - для наглядности
cat ${FILE_REPSTAT}
### --- ЗАПУСК через функцию
## вызов функции с параметрами для открепления и удаления таблиц
alter_table ${FILE_REPSTAT} $REPSTAT

### ----------------------------- REPORTER_JOURNAL -------------------------------
echo "<<<---ОЧИСТКА REPORTER_JOURNAL..."
# Формирование списка таблиц старше 13-ти месяцев на удаление.
db2look -d mnt -e -t $REPJOUR | grep ${REPJOUR}"_PART_" | grep -v ${REPJOUR}"_PART_"${CUR_YEAR} | cut -d " " -f 3 | sed -e 's/^"//' -e 's/"$//' | head -n -$MONTH_NUM > ${FILE_REPJOUR}
cat ${FILE_REPJOUR}
## вызов функции с параметрами для открепления и удаления таблиц
alter_table ${FILE_REPJOUR} $REPJOUR

### ----------------------------- REPORTER_DETAILS -------------------------------
echo "<<<---ОЧИСТКА REPORTER_DETAILS..."
# Формирование списка таблиц старше 13-ти месяцев на удаление.
db2look -d mnt -e -t $REPDET | grep ${REPDET}"_PART_" | grep -v ${REPDET}"_PART_"${CUR_YEAR} | cut -d " " -f 3 | sed -e 's/^"//' -e 's/"$//' | head -n -$MONTH_NUM > ${FILE_REPDET}
cat ${FILE_REPDET}
## вызов функции с параметрами для открепления и удаления таблиц
alter_table ${FILE_REPDET} $REPDET

### ----------------------------- REP_AUDIT_OWNERGID -------------------------------
echo "<<<---ОЧИСТКА REP_AUDIT_OWNERGID..."
# Формирование списка таблиц старше 13-ти месяцев на удаление.
db2look -d mnt -e -t $REPAOGID | grep ${REPAOGID}"_PART_" | grep -v ${REPAOGID}"_PART_"${CUR_YEAR} | cut -d " " -f 3 | sed -e 's/^"//' -e 's/"$//' | head -n -$MONTH_NUM > ${FILE_REPAOGID}
cat ${FILE_REPAOGID}
## вызов функции с параметрами для открепления и удаления таблиц
alter_table ${FILE_REPAOGID} $REPAOGID

### ----------------------------- REP_AUDIT_OWNERUID -------------------------------
echo "<<<---ОЧИСТКА REP_AUDIT_OWNERUID..."
# Формирование списка таблиц старше 13-ти месяцев на удаление.
db2look -d mnt -e -t $REPAOUID | grep ${REPAOUID}"_PART_" | grep -v ${REPAOUID}"_PART_"${CUR_YEAR} | cut -d " " -f 3 | sed -e 's/^"//' -e 's/"$//' | head -n -$MONTH_NUM > ${FILE_REPAOUID}
cat ${FILE_REPAOUID}
## вызов функции с параметрами для открепления и удаления таблиц
alter_table ${FILE_REPAOUID} $REPAOUID

### ----------------------------- REP_AUDIT_CORR -------------------------------
echo "<<<---ОЧИСТКА REP_AUDIT_CORR..."
# Формирование списка таблиц старше 13-ти месяцев на удаление.
db2look -d mnt -e -t $REPACORR | grep ${REPACORR}"_PART_" | grep -v ${REPACORR}"_PART_"${CUR_YEAR} | cut -d " " -f 3 | sed -e 's/^"//' -e 's/"$//' | head -n -$MONTH_NUM > ${FILE_REPACORR}
cat ${FILE_REPACORR}
## вызов функции с параметрами для открепления и удаления таблиц
alter_table ${FILE_REPACORR} $REPACORR

### ----------------------------- REP_AUDIT_SEVERITY -------------------------------
echo "<<<---ОЧИСТКА REPORTER_JOURNAL..."
# Формирование списка таблиц старше 13-ти месяцев на удаление.
db2look -d mnt -e -t $REPASEV | grep ${REPASEV}"_PART_" | grep -v ${REPASEV}"_PART_"${CUR_YEAR} | cut -d " " -f 3 | sed -e 's/^"//' -e 's/"$//' | head -n -$MONTH_NUM > ${FILE_REPASEV}
cat ${FILE_REPASEV}
## вызов функции с параметрами для открепления и удаления таблиц
alter_table ${FILE_REPASEV} $REPASEV

### ----------------------------- REP_AUDIT_ACK -------------------------------
echo "<<<---ОЧИСТКА REP_AUDIT_ACK..."
# Формирование списка таблиц старше 13-ти месяцев на удаление.
db2look -d mnt -e -t $REPAACK | grep ${REPAACK}"_PART_" | grep -v ${REPAACK}"_PART_"${CUR_YEAR} | cut -d " " -f 3 | sed -e 's/^"//' -e 's/"$//' | head -n -$MONTH_NUM > ${FILE_REPAACK}
cat ${FILE_REPAACK}
## вызов функции с параметрами для открепления и удаления таблиц
alter_table ${FILE_REPAACK} $REPAACK


####### -------------------------------------------- TABLESPACE REDUCE MAX -------------------------------------------- #######
#  Затем необходимо высвободить место на диске
#+ Для этого очищаем TABLESPAC'ы, основные:
echo ">>>---TABLESPACE REDUCE MAX "
echo "<<<---TSDATA32K REDUCE MAX"
db2 "ALTER TABLESPACE TSDATA32K REDUCE MAX"
echo "<<<---TSDATA16K REDUCE MAX"
db2 "ALTER TABLESPACE TSDATA16K REDUCE MAX"
echo "<<<---TSDATA8K REDUCE MAX"
db2 "ALTER TABLESPACE TSDATA8K REDUCE MAX"

###
db2 "select * from table(SYSPROC.MON_GET_EXTENT_MOVEMENT_STATUS('', -1))"

## Запуск функции с параметрами для проверки
#+ состояния выполнения REDUCE MAX
get_extent_movement_status TSDATA16K
get_extent_movement_status TSDATA32K
get_extent_movement_status TSDATA8K

###
db2 "select * from table(SYSPROC.MON_GET_EXTENT_MOVEMENT_STATUS('', -1))"

####### ----------- DISCONNECT and EXIT ----------- #######

## После завершения возвращаем ИБД в рабочий режим
db2 uncatalog database mnt
db2 catalog database reporter on "/"$LDD
db2stop force
db2set DB2_PMODEL_SETTINGS=MAX_BACKGROUND_SYSAPPS:500

db2start

## Проверка на коннект к верному алиасу БД
DB_ALIAS=$(db2 list db directory | grep REPORTER -A2 | grep "Database alias" | cut -d "=" -f 2)
echo $DB_ALIAS
if [ "$DB_ALIAS" != " REPORTER"  ]; then
  echo ">>>---алиас БД не вернулся на REPORTER !!! Значит что-то пошло не так. Нужно перепроверить вручную."
#  exit 1
else
  echo ">>>---OK. алиас БД REPORTER"
fi

echo ">>>---disconnect from reporter and exit"

## Удаление временных файлов
echo "<<<---Удаление временных файлов..."
rm *.temp_hdbc

echo "<<<------ Выполнение сценария автоматической очитски ИБД прошло успешно. ------>>>"
#exit
