#!/bin/bash

#####################################################################################################
## Этот скрипт предназначен для очистки Исторической БД во время выполнения плановых работ.
## Запускается с параметрами и выполняется самостоятельно.
## Все логи выполнения выводятся на монитор во время выполнения.
##
## Разработан Капущак М.М.
## Начало разработки 26.06.2023
##
## Version 2.43
##
## Вызывать данный сценарий необходимо в таком виде: ./$0 arg1 arg2 arg3 ... argN
## Например: ./$0 REP_AUDIT_OWNERGID REPORTER_JOURNAL ..
## где ./$0 - имя сценария (имя может быть изменено). Также можно запускать и без аргументов,
## потому что основные таблицы для очистки уже записаны в массив в сценарии.
## Количество аргументов может быть более одного. Аргументами являются названия таблиц,
## предназначенных для очистки
##
## ВНИМАНИЕ!!! Ниже в разделе OPTIONS есть важная информация по настройке запуска сценария.
## Просьба внимательно ознакомиться с ней перед запуском.
##
#####################################################################################################

echo "####### -------------------------------------------- START SCRIPT -------------------------------------------- #######"
TIME_START=$(date '+%H:%M:%S')
echo "Время начала: " $TIME_START

####### -------------------------------------------- OPTIONS -------------------------------------------- #######
## Настройки выполнения сценария.
#+ Необходимо для ручной настройки параметрров выполнения 
#+ сценария.
#+ Внимание! Следует быть очень внимательным
#+ при указании настроек данных.

MONTH_COUNT=12        # Количество месяцев сохранения актуальных данных от текущего месяца
S_TIME=300             # Интервал времени между проверками на статус выполнения операции TABLESPACE ... REDUCE (в секундах)
DB2INST1="db2inst1"   # Пользователь БД

## Внимание!!! В функции alter_table() существует два режима выполнения: для Теста и для Прода. 
#+ Просьба учитывать при запуске и 
#+ ознакомиться с ними в самой функции ниже.

####### -------------------------------------------- VARS, FUNCTIONS & OTHER -------------------------------------------- #######
####### ----------- VARS ----------- #######

SCRIPT=$0
CUR_USER=whoami

## Предопределённый список таблиц для очистки
#+ При внесении сюда новых названий или удалении старых
#+ необходимо так же добавить и\или удалить 
#+ в\из массива --->>> TABLES_ARRAY
REPSTAT=REPORTER_STATUS
REPJOUR=REPORTER_JOURNAL
REPDET=REPORTER_DETAILS
REPAOGID=REP_AUDIT_OWNERGID
REPAOUID=REP_AUDIT_OWNERUID
REPACORR=REP_AUDIT_CORR    #*
REPASEV=REP_AUDIT_SEVERITY    #*
REPAACK=REP_AUDIT_ACK

CUR_MONTH=$(date '+%m' | sed 's/^0//')    # текущий месяц
CUR_YEAR=$(date '+%Y')    # текущий год
let "CUR_MONTH -= 1"    # текущий месяц - 1. Переменная используется для сохранения 13ти (год+месяц) последних месяцев и удаления самых старых
MONTH_NUM=$((MONTH_COUNT-CUR_MONTH))    # номер месяца для отсчёта

echo $CUR_MONTH
echo $CUR_YEAR
echo $MONTH_NUM

## Этот парамет используется время отдыха между проверками на "db2 list utilities show detail" & "TABLESPACE ... REDUCE MAX"
#+ Время можно изменить при необходимости. Например, увеличить, если "TABLESPACE ... REDUCE MAX" идёт слишком долго
#S_TIME=60 - ПЕРЕНЕСЁН в раздел настроек.
echo "DEBUG -------------> Время ожидания" ${S_TIME}

####### ----------- FUNCTIONS ----------- #######
#+ В этом блоке объявляются функции для исключения повторяющихся операций.
#+ Все описания прилагаются.

## функция для перебора таблиц из массива 
#+ без входных параметров
select_table()
{
  for table in "${TABLES_ARRAY[@]}";
  do
    # Создание временного файла со списком партиций по выбранной таблице для удаления
    FILE_=$table"_PART_"${CUR_YEAR}.temp_hdbc    # temp_hdbc - означает temporary_history-database-clean
    echo "<<<---ОЧИСТКА ${table}..."
    # Формирование списка таблиц старше 13-ти месяцев на удаление.
    echo "DEBUG ------ # Формирование списка таблиц старше 13-ти месяцев на удаление."
    echo "db2look -d mnt -e -t $table | grep ${table}"_PART_" | grep -v ${table}"_PART_"${CUR_YEAR} | cut -d " " -f 3 | sed -e 's/^"//' -e 's/"$//' |  head -n -$MONTH_NUM > ${FILE_} "
    db2look -d mnt -e -t $table | grep ${table}"_PART_" | grep -v ${table}"_PART_"${CUR_YEAR} | cut -d " " -f 3 | sed -e 's/^"//' -e 's/"$//' | head -n -$MONTH_NUM > ${FILE_}
    cat ${FILE_}
    ## вызов функции с параметрами для открепления и удаления таблиц
    echo "alter_table ${FILE_} $table "
    alter_table ${FILE_} $table
  done
}


## Функция с входными параметрами для открепления партиций и удаления таблиц.
#+ первый параметр - имя файла со списком партиций
#+ второй параметр - имя таблицы
alter_table()
{
  for TABLE_PART in $(cat "$1");          # Используется в прод-режиме для удаления всех неактуальных таблиц старше 13ти месяцев.
#  for TABLE_PART in $(head -n 1 "$1");    # Используется в тест-режиме с указанием числа таблиц для удаления из списка старше 13ти месяцев.
  do
    echo "<<<---detach partition ${TABLE_PART}...";
    echo "alter table "${2}"  detach partition " ${TABLE_PART}" into table " ${TABLE_PART} " && db2  drop table " ${TABLE_PART} 
    db2 "alter table "$2"  detach partition ${TABLE_PART} into table ${TABLE_PART} " && db2 " drop table ${TABLE_PART} " ;
    echo ">>>---get state - db2 list utilities show detail "
      state=$(db2 list utilities show detail | grep -i State | cut -d "=" -f 2)
      echo $state " for debug"

      while [ $state != "00000" ]
      do
        echo "<<<---sleep ${S_TIME} - Ожидаем выполнения предыдущих операций alter table & drop table"
        sleep $S_TIME
        echo ">>>---get state - db2 list utilities show detail (in While) for debug"
        state=$(db2 list utilities show detail | grep -i State | cut -d "=" -f 2)
        echo $state " (in While) for debug"
      done;
  done
}

## Функция с входными параметрами для проверки состояния выполнения
#+ запроса TABLESPACE ... REDUCE MAX.
#+ первый параметр - имя TABLESPACE
get_extent_movement_status()
{
echo "select TBSP_NAME, NUM_EXTENTS_LEFT from table(SYSPROC.MON_GET_EXTENT_MOVEMENT_STATUS('', -1))"
db2 "select TBSP_NAME, NUM_EXTENTS_LEFT from table(SYSPROC.MON_GET_EXTENT_MOVEMENT_STATUS('', -1))" | grep ${1} | sed 's/  */ /g' | cut -d " " -f 2 > ${1}.temp_hdbc

echo ">>>---in ${1} NUM_EXTENTS_LEFT"
echo ">>>---${1} "
for NUM_EXTENTS_LEFT in $(cat ${1}.temp_hdbc);
do
  echo $NUM_EXTENTS_LEFT
  if (( $NUM_EXTENTS_LEFT != '-1' )); then
    echo "!!!---Не равняется (-1)= " $NUM_EXTENTS_LEFT
    echo "<<<---sleep ${S_TIME} - Ожидаем завершения TABLESPACE ${1} REDUCE MAX "
    sleep $S_TIME
    while (( $NUM_EXTENTS_LEFT != '-1' ))
    do
      echo ">>>---get NUM_EXTENTS_LEFT (in While)"
      db2 "select TBSP_NAME, NUM_EXTENTS_LEFT from table(SYSPROC.MON_GET_EXTENT_MOVEMENT_STATUS('', -1))" | grep ${1} | sed 's/  */ /g' | cut -d " " -f 2 > ${1}.temp_hdbc
      NUM_EXTENTS_LEFT=$(cat ${1}.temp_hdbc)
      echo ">>>---NUM_EXTENTS_LEFT=${NUM_EXTENTS_LEFT} (in While)"
      echo "<<<---sleep ${S_TIME} - Ожидаем завершения TABLESPACE ${1} REDUCE MAX (in While)"
      sleep $S_TIME
    done;
  else
    echo ">>>---Равняется (-1)= " $NUM_EXTENTS_LEFT
  fi;
done
}


### ----------- DEBUG

####### Данный раздел потерял актулаьность. OLD
####### ----------- SWITCH user to db2inst1 ----------- #######

if [ $(whoami) != "$DB2INST1" ]
then
#  echo "<<<---SWITCH user to db2inst1. Please enter password..."
#  sudo su - db2inst1
  echo ">>>---Текущий пользователь не db2inst1. Запустите сценарий под оболочкой db2inst1."
  exit 1
else
  echo "<<<---Текущий пользователь db2inst1. Продолжаем выполнение..."
fi


####### -------------------------------------------- BEGIN CONNECT  -------------------------------------------- #######

echo "<<<------ Начало выполнения сценария автоматической очистки ИБД...  ------>>>"

####### ----------- CONNECT ----------- #######
echo "<<<---connect to reporter..."
db2 connect to REPORTER

####### ----------- UNCATALOG - CATATLOG ----------- #######
echo "<<<---change mode to (mnt)..."
## получение точки монтирования БД
echo "----> получение точки монтирования БД"
LDD=$(db2 list db directory | grep "REPORTER" -A2 | grep "Local database directory" | cut -d "=" -f 2 | sed  's/ //')
echo "----> Точка монтирования БД: " $LDD
## меняем алиас БД
echo "<<<--- меняем алиас БД - db2 catalog database REPORTER as mnt on " $LDD
db2 uncatalog database REPORTER
db2 catalog database REPORTER as mnt on $LDD
db2stop force
db2set DB2_PMODEL_SETTINGS=MAX_BACKGROUND_SYSAPPS:1
db2start
####### ----------- CONNECT ----------- #######
db2 connect to mnt

## Проверка на коннект к новому расположению БД
echo "----> Проверка на коннект к новому расположению БД "
echo " db2 list db directory | grep "MNT" -A2  "
DB_ALIAS=$(db2 list db directory | grep "MNT" -A2 | grep "Database alias" | cut -d "=" -f 2 | head -1 )
echo $DB_ALIAS
if [ "$DB_ALIAS" != " MNT"  ]; then
  echo "!!!---алиас БД не изменился на MNT !!! Значит что-то пошло не так. Нужно перепроверить вручную."
  exit 1
else
  echo "<<<---OK. Алиас БД = MNT"
fi

####### -------------------------------------------- START clean -------------------------------------------- #######

### ----------- LIST partitions

## получение списка партиций для удаления кроме последних 13 месяцев 
#+ для всех таблиц

## Проверка на наличие входящих аргументов:
#+ если аргументы есть, то формируется новый массив с этими аргументами,
#+ при их отсутствии формируется массив с предопределёнными
#+ таблицами (выбираются все имеющиеся)
if [ ! -n "$1" ]
then
  echo "Параметров нет и сценарий будет выполняться с предопределёнными значениями таблиц:"
  TABLES_ARRAY=($REPSTAT $REPJOUR $REPDET $REPAOGID $REPAOUID $REPACORR $REPASEV $REPAACK)
  echo ${TABLES_ARRAY[@]}
else
  echo "Параметр есть. Список таблиц, поступивших в качестве аргументов:"
  TABLES_ARRAY=($@)
  for arg in "$@"
  do
    echo "Таблица #$index = $arg"
    let "index+=1"
  done
fi

## Вызов функции с перебором таблиц
select_table


####### -------------------------------------------- TABLESPACE REDUCE MAX -------------------------------------------- #######
#  Затем необходимо высвободить место на диске
#+ Для этого очищаем TABLESPAC'ы, основные:
echo "----> Необходимо высвободить место на диске"
echo "----> Для этого очищаем TABLESPAC'ы, основные:"
echo ">>>---TABLESPACE REDUCE MAX "
echo "<<<---TSDATA32K REDUCE MAX"
db2 "ALTER TABLESPACE TSDATA32K REDUCE MAX"
echo "<<<---TSDATA16K REDUCE MAX"
db2 "ALTER TABLESPACE TSDATA16K REDUCE MAX"
echo "<<<---TSDATA8K REDUCE MAX"
db2 "ALTER TABLESPACE TSDATA8K REDUCE MAX"

###
db2 "select * from table(SYSPROC.MON_GET_EXTENT_MOVEMENT_STATUS('', -1))"

## Запуск функции с параметрами для проверки
#+ состояния выполнения REDUCE MAX
get_extent_movement_status TSDATA16K
get_extent_movement_status TSDATA32K
get_extent_movement_status TSDATA8K

###
db2 "select * from table(SYSPROC.MON_GET_EXTENT_MOVEMENT_STATUS('', -1))"

####### ----------- DISCONNECT and EXIT ----------- #######

## После завершения возвращаем ИБД в рабочий режим
echo "----> После завершения возвращаем ИБД в рабочий режим"
db2 uncatalog database mnt
echo "----> db2 catalog database reporter on " $LDD
db2 catalog database reporter on $LDD
db2stop force
db2set DB2_PMODEL_SETTINGS=MAX_BACKGROUND_SYSAPPS:500

db2start

## Проверка на коннект к верному алиасу БД
#DB_ALIAS=$(db2 list db directory | grep "REPORTER" -A2 | grep "Database alias" | cut -d "=" -f 2)
DB_ALIAS=$(db2 list db directory | grep "REPORTER" -A2 | grep "Database alias" | cut -d "=" -f 2 | sed  's/ //')
echo "----> алиас БД: "$DB_ALIAS
if [ "$DB_ALIAS" != "REPORTER"  ]; then
  echo ">>>---алиас БД не вернулся на REPORTER !!! Значит что-то пошло не так. Нужно перепроверить вручную."
#  exit 1
else
  echo ">>>---OK. алиас БД REPORTER"
fi

## Пожелание 2: в конец скрипта после завершения всех операций добавить строку
echo "<<<---connect to reporter..."
db2 connect to REPORTER
echo "<<<--- RUNSTATS ON TABLE DB2INST1.REPORTER_STATUS... "
time db2 "RUNSTATS ON TABLE DB2INST1.REPORTER_STATUS WITH DISTRIBUTION AND DETAILED INDEXES ALL"

echo ">>>---disconnect from reporter and exit"

## Удаление временных файлов
echo "<<<---Удаление временных файлов..."
#rm *.temp_hdbc        # ВРЕМЕННО отключил для дебага

echo "<<<------ Выполнение сценария автоматической очистки ИБД прошло успешно. ------>>>"
#exit

echo "####### -------------------------------------------- END SCRIPT -------------------------------------------- #######"
TIME_END=$(date '+%H:%M:%S')
echo "Время завершения: " $TIME_END

